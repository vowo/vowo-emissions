package emissions

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"

	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"gitlab.com/vowo/vowo-corelib/pkg/httperr"
	"gitlab.com/vowo/vowo-corelib/pkg/httputils"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"go.uber.org/zap"
)

// MakeHandler returns a handler for the emissions service.
func (s *Server) MakeHandler(endpoints *Set) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(s.EncodeError),
	}

	getForWaypointHandler := kithttp.NewServer(
		endpoints.getForWaypoint,
		decodeGetForWaypointRequest,
		httputils.EncodeHTTPGenericResponse,
		append(opts, kithttp.ServerBefore(tracing.HTTPToContext(s.tracer, "getEmissionsForWaypoint", s.logger)))...,
	)

	r := mux.NewRouter()
	r.Handle("/", getForWaypointHandler).Methods("POST")
	r.NotFoundHandler = http.HandlerFunc(httperr.HandleNotFound)
	r.MethodNotAllowedHandler = http.HandlerFunc(httperr.HandleMethodNotAllowed)

	return r
}

func decodeGetForWaypointRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req emissionsRequest
	if e := json.NewDecoder(r.Body).Decode(&req); e != nil {
		return nil, e
	}
	return req, nil
}

func encodeGetForWaypointRequest(ctx context.Context, r *http.Request, request interface{}) error {
	return encodeRequest(ctx, r, request)
}

func decodeGetForWaypointResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}

	var resp emissionsResponse
	err := json.NewDecoder(r.Body).Decode(&resp)
	return resp, err
}

// encodeRequest likewise JSON-encodes the request to the HTTP request body.
// Don't use it directly as a transport/http.Client EncodeRequestFunc:
// profilesvc endpoints require mutating the HTTP method and request path.
func encodeRequest(_ context.Context, req *http.Request, request interface{}) error {
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(request)
	if err != nil {
		return err
	}
	req.Body = ioutil.NopCloser(&buf)
	return nil
}

// EncodeError encode errors from business-logic
func (s *Server) EncodeError(ctx context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	innerErr := errors.Cause(err)
	if innerErr == nil {
		innerErr = err
	}

	switch innerErr {
	case errInvalidArgument:
		s.logger.For(ctx).Info("bad request", zap.Error(err))
		w.WriteHeader(http.StatusBadRequest)
	default:
		s.logger.For(ctx).Fatal("server error", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
	}
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
