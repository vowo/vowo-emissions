package emissions

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/suite"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

type EmissionsTestSuite struct {
	suite.Suite
}

func (suite *EmissionsTestSuite) TestGetEmissionsByWaypoint() {
	//arrange
	waypoint := &Waypoint{
		Source: &Coord{
			Lat: 46.9480,
			Lon: 7.4474,
		},
		Destination: &Coord{
			Lat: 47.3769,
			Lon: 8.5417,
		},
		TransportType: Ship,
	}

	amountInG := uint64(875)
	service := NewService(nil, log.Factory{})

	//act
	emissions, err := service.GetEmissionsForWaypoint(context.Background(), waypoint, amountInG)

	//assert
	require.NoError(suite.T(), err)
	require.NotNil(suite.T(), emissions)

	distanceInKm := Distance(waypoint.Source, waypoint.Destination)
	coTwoInMgPerKmAndG := (float64(shipCoTwoGPerTon) / float64(1e6)) * float64(1e3)
	expectedEmission := uint64(coTwoInMgPerKmAndG * distanceInKm * float64(amountInG))
	assert.Equal(suite.T(), expectedEmission, emissions.CoTwoInMg)
	assert.Equal(suite.T(), distanceInKm, emissions.DistanceInKm)
}

func TestEmissionsTestSuite(t *testing.T) {
	suite.Run(t, new(EmissionsTestSuite))
}
