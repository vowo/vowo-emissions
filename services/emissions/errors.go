package emissions

import "errors"

var errInvalidArgument = errors.New("invalid argument")
var errInvalidTransportType = errors.New("invalid transport type")
