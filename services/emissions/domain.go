package emissions

// Waypoint domain model
type Waypoint struct {
	Source        *Coord        `json:"source"`
	Destination   *Coord        `json:"destination"`
	TransportType TransportType `json:"transportType"`
}

// Coord represents a geographic coordinate.
type Coord struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}

// Emission represents the emissions for a waypoint
type Emission struct {
	CoTwoInMg    uint64  `json:"coTwoInMg"`
	DistanceInKm float64 `json:"distanceInKm"`
}

type TransportType int

const (
	Other    TransportType = 0
	Truck    TransportType = 1
	Ship     TransportType = 2
	Airplane TransportType = 3
	Train    TransportType = 4
)

func (tt TransportType) String() string {
	names := [...]string{
		"Truck",
		"Ship",
		"Airplane",
		"Train",
	}

	if tt < Truck || tt > Train {
		return "Other"
	}

	return names[tt]
}
