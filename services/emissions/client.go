package emissions

import (
	"net/url"
	"time"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	kitopentracing "github.com/go-kit/kit/tracing/opentracing"
	httptransport "github.com/go-kit/kit/transport/http"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/sony/gobreaker"
	"gitlab.com/vowo/vowo-corelib/pkg/httputils"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
)

// NewHTTPClient creates a new emissions.Service backed by an HTTP server living at the
// remote instance. Instance is expected to come from a service discovery system,
// so likely of the form "host:port".
func NewHTTPClient(instance string, tracer opentracing.Tracer, logger log.Factory) (Service, error) {
	instance = httputils.HTTPPrefixURL(instance)
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	options := []httptransport.ClientOption{}
	var getForWaypoint endpoint.Endpoint
	{
		getForWaypoint = httptransport.NewClient(
			"POST",
			httputils.AddPathToURL(u, ""),
			encodeGetForWaypointRequest,
			decodeGetForWaypointResponse,
			append(options, httptransport.ClientBefore(tracing.ContextToHTTP(tracer, logger)))...,
		).Endpoint()
		getForWaypoint = kitopentracing.TraceClient(tracer, "emissions")(getForWaypoint)
		getForWaypoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "emissions",
			Timeout: 30 * time.Second,
		}))(getForWaypoint)
	}

	return &Set{
		getForWaypoint: getForWaypoint,
	}, nil
}
