package emissions

import (
	"context"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	kitopentracing "github.com/go-kit/kit/tracing/opentracing"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/sony/gobreaker"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

// emissionsRequest for an http request
type emissionsRequest struct {
	Waypoint  *Waypoint `json:"waypoint"`
	AmountInG uint64    `json:"amountInG"`
}

// emissionsResponse to an http request
type emissionsResponse struct {
	Emission *Emission `json:"emission"`
	Err      error     `json:"-"` // should be intercepted by Failed/errorEncoder
}

// Set collects all of the endpoints that compose an emissions service.
type Set struct {
	getForWaypoint endpoint.Endpoint
}

// GetEmissionsForWaypoint implements the emissions.Service
func (s *Set) GetEmissionsForWaypoint(ctx context.Context, waypoint *Waypoint, amountInG uint64) (*Emission, error) {
	resp, err := s.getForWaypoint(ctx, emissionsRequest{Waypoint: waypoint, AmountInG: amountInG})
	if err != nil {
		return nil, err
	}

	response := resp.(emissionsResponse)
	return response.Emission, response.Err
}

// NewEndpoint creates a new emissions endpoint
func NewEndpoint(s Service, tracer opentracing.Tracer, logger log.Factory) *Set {
	var getForWaypoint endpoint.Endpoint
	{
		getForWaypoint = makeGetForWaypointEndpoint(s)
		getForWaypoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(getForWaypoint)
		getForWaypoint = kitopentracing.TraceServer(tracer, "emissions")(getForWaypoint)
		getForWaypoint = LoggingMiddleware(logger)(getForWaypoint)
	}

	return &Set{
		getForWaypoint: getForWaypoint,
	}
}

func makeGetForWaypointEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(emissionsRequest)
		emission, err := s.GetEmissionsForWaypoint(ctx, req.Waypoint, req.AmountInG)
		if err != nil {
			return nil, err
		}

		return &emissionsResponse{Emission: emission}, nil
	}
}

// compile time assertions for our response types implementing endpoint.Failer.
var (
	_ endpoint.Failer = emissionsResponse{}
)

// Failed implements endpoint.Failer.
func (r emissionsResponse) Failed() error { return r.Err }
