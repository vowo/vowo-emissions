package emissions

import (
	"context"
	"fmt"
	"math"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

// Service is the interface to access emissions.
type Service interface {
	GetEmissionsForWaypoint(ctx context.Context, waypoint *Waypoint, amountInG uint64) (*Emission, error)
}

type service struct {
	tracer opentracing.Tracer
	logger log.Factory
}

// TODO find more accurate way to calculate this
// these values were retrieved from https://timeforchange.org/co2-emissions-shipping-goods
const (
	// Determines the value of co2 in gram per km for one ton of goods
	shipCoTwoGPerTon     = uint(25)
	airplaneCoTwoGPerTon = uint(250)
	truckCoTwoGPerTon    = uint(105)
	trainCoTwoGPerTon    = uint(65)
)

var (
	shipCoTwoMgPerG     = toMgPerGram(shipCoTwoGPerTon)
	airplaneCoTwoMgPerG = toMgPerGram(airplaneCoTwoGPerTon)
	truckCoTwoMgPerG    = toMgPerGram(truckCoTwoGPerTon)
	trainCoTwoMgPerG    = toMgPerGram(trainCoTwoGPerTon)
)

func toMgPerGram(gramPerTon uint) float64 {
	return (float64(gramPerTon) * float64(1e3)) / float64(1e6) // (value in gram * milligram coeff) / to gram  per ton of freight and per km
}

func coTwoInMgforKmAndAmount(coTwoPerMg float64, distanceInKm float64, amountInG uint64) uint64 {
	return uint64(coTwoPerMg * distanceInKm * float64(amountInG))
}

func (s *service) GetEmissionsForWaypoint(ctx context.Context, waypoint *Waypoint, amountInG uint64) (*Emission, error) {
	//TODO take actual distance not airline
	distanceInKm := Distance(waypoint.Source, waypoint.Destination)

	switch waypoint.TransportType {
	case Airplane:
		co2inMg := coTwoInMgforKmAndAmount(airplaneCoTwoMgPerG, distanceInKm, amountInG)
		return &Emission{CoTwoInMg: co2inMg, DistanceInKm: distanceInKm}, nil
	case Ship:
		co2inMg := coTwoInMgforKmAndAmount(shipCoTwoMgPerG, distanceInKm, amountInG)
		return &Emission{CoTwoInMg: co2inMg, DistanceInKm: distanceInKm}, nil
	case Train:
		co2inMg := coTwoInMgforKmAndAmount(trainCoTwoMgPerG, distanceInKm, amountInG)
		return &Emission{CoTwoInMg: co2inMg, DistanceInKm: distanceInKm}, nil
	case Truck:
		co2inMg := coTwoInMgforKmAndAmount(truckCoTwoMgPerG, distanceInKm, amountInG)
		return &Emission{CoTwoInMg: co2inMg, DistanceInKm: distanceInKm}, nil
	default:
		return nil, errors.Wrap(errInvalidTransportType, fmt.Sprintf("type: %v", waypoint.TransportType))
	}
}

// degreesToRadians converts from degrees to radians.
func degreesToRadians(d float64) float64 {
	return d * math.Pi / 180
}

const (
	earthRaidusM = 6371 // radius of the earth in kilometers.
)

// Distance calculates the shortest path between two coordinates on the surface
// of the Earth.
// The distance returned is in kilometers
// http://en.wikipedia.org/wiki/Haversine_formula
func Distance(p, q *Coord) (km float64) {
	lat1 := degreesToRadians(p.Lat)
	lon1 := degreesToRadians(p.Lon)
	lat2 := degreesToRadians(q.Lat)
	lon2 := degreesToRadians(q.Lon)

	diffLat := lat2 - lat1
	diffLon := lon2 - lon1

	a := math.Pow(math.Sin(diffLat/2), 2) + math.Cos(lat1)*math.Cos(lat2)*
		math.Pow(math.Sin(diffLon/2), 2)

	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	km = c * earthRaidusM
	return km
}

// NewService creates an emissions service with necessary dependencies.
func NewService(tracer opentracing.Tracer, logger log.Factory) Service {
	return &service{
		logger: logger,
		tracer: tracer,
	}
}
