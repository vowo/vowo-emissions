package cmd

import (
	"net"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"gitlab.com/vowo/vowo-emissions/services/emissions"
	"go.uber.org/zap"
)

// emissionsCommand represents the emissions command
var emissionsCommand = &cobra.Command{
	Use:   "emissions",
	Short: "Starts emissions service",
	Long:  `Starts emissions service.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		zapLogger := logger.With(zap.String("service", "emissions"))
		logger := log.NewFactory(zapLogger)
		server := emissions.NewServer(
			net.JoinHostPort(emissionsOptions.serverInterface, strconv.Itoa(emissionsOptions.serverPort)),
			net.JoinHostPort(emissionsOptions.serverInterface, strconv.Itoa(emissionsOptions.serverHealthPort)),
			tracing.Init("emissions", metricsFactory.Namespace("emissions", nil), logger, jAgentHostPort),
			metricsFactory,
			logger,
			jAgentHostPort,
		)

		go server.RunHealthAndReadinessProbes()
		return logError(zapLogger, server.Run())
	},
}

var (
	emissionsOptions struct {
		serverInterface  string
		serverPort       int
		serverHealthPort int
	}
)

func init() {
	RootCmd.AddCommand(emissionsCommand)

	//TODO read port from env
	emissionsCommand.Flags().StringVarP(&emissionsOptions.serverInterface, "bind", "", "0.0.0.0", "interface to which the emissions server will bind")
	emissionsCommand.Flags().IntVarP(&emissionsOptions.serverPort, "port", "p", 8085, "port on which the emissions server will listen")
	emissionsCommand.Flags().IntVarP(&emissionsOptions.serverHealthPort, "healthPort", "e", 8185, "port on which the health check of the emissions service will listen")
}
